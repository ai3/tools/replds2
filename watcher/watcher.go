package watcher

import (
	"context"
	"errors"
	"io"
	"log"
	"time"

	replds "git.autistici.org/ai3/tools/replds2"
	"git.autistici.org/ai3/tools/replds2/common"
	pb "git.autistici.org/ai3/tools/replds2/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// A TriggerManager is both source and executor of triggers
// (notifications in common/notify.go terms).
type TriggerManager interface {
	common.NotifySource
	Notify(*common.NotifyBatch)
}

// The Watcher is an asynchronous replication client that can operate
// on a subtree of the remote database. It will watch for changes and
// propagate them to the local database with low latency.
//
// It also has the capability of executing path-specific triggers upon
// changes.
type Watcher struct {
	path     string
	store    replds.Store
	triggers TriggerManager
	conn     *grpc.ClientConn
}

func New(conn *grpc.ClientConn, store replds.Store, path string, triggers TriggerManager) *Watcher {
	if triggers == nil {
		triggers = new(nullTriggerManager)
	}
	return &Watcher{
		path:     path,
		conn:     conn,
		store:    store,
		triggers: triggers,
	}
}

// Run the watcher. The replication algorithm is less terrible than
// the internal synchronization protocol:
//
// - establish a GRPC streaming connection to a server
// - send a summary of the local database state
// - receive all the missing Node objects
// - keep receiving further updates forever (as long as the streaming
//   connection remains active).
//
// This works because as long as we're connected to the same server,
// we know with certainty the state of both local and remote
// databases, and we can just push incremental updates from the server
// rather than running periodic (expensive) synchronizations. As a
// results, Watchers are comparatively lightweight, beyond the initial
// synchronization step.
//
func (w *Watcher) Run(ctx context.Context) {
	stub := pb.NewRepldsClient(w.conn)
	for {
		// Establish the streaming connection.
		req := &pb.WatchRequest{
			BasePath: w.path,
			Summary:  w.store.Summary(),
		}
		stream, err := stub.Watch(ctx, req)
		if errors.Is(err, context.Canceled) || status.Code(err) == codes.Canceled {
			return
		}
		if err != nil {
			log.Printf("watch error: %v", err)
			goto retry
		}

		for {
			resp, err := stream.Recv()
			if errors.Is(err, io.EOF) || status.Code(err) == codes.Canceled {
				break
			}
			if err != nil {
				log.Printf("watch stream receive error: %v", err)
				break
			}

			// Run triggers for each batch.
			tb := common.NewNotifyBatch(w.triggers)
			for _, node := range resp.Nodes {
				if ok, err := w.store.AddNode(node); err == nil && ok {
					tb.Add(node)
				}
			}
			w.triggers.Notify(tb)
		}

	retry:
		time.Sleep(1 * time.Second)
	}

}
