//go:generate protoc --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative replds.proto internal.proto
package proto

func (n *Node) Meta() *NodeMeta {
	return &NodeMeta{
		Path:    n.Path,
		Version: n.Version,
	}
}

func (n *Node) WithoutData() *Node {
	return &Node{
		Path:      n.Path,
		Version:   n.Version,
		Deleted:   n.Deleted,
		Timestamp: n.Timestamp,
	}
}

func (n *Node) WithData(data []byte) *Node {
	return &Node{
		Path:      n.Path,
		Version:   n.Version,
		Deleted:   n.Deleted,
		Timestamp: n.Timestamp,
		Data:      data,
	}
}
