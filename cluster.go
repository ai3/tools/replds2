package replds

import (
	"math/rand"

	"google.golang.org/grpc"
)

type grpcClusterMgr struct {
	peers   map[string]*grpc.ClientConn
	targets []string
}

func NewGRPCCluster(peers []string, opts ...grpc.DialOption) (ClusterMgr, error) {
	m := make(map[string]*grpc.ClientConn)
	var targets []string
	for _, target := range peers {
		conn, err := grpc.Dial(target, opts...)
		if err != nil {
			return nil, err
		}
		m[target] = conn
		targets = append(targets, target)
	}
	return &grpcClusterMgr{
		peers:   m,
		targets: targets,
	}, nil
}

func (c *grpcClusterMgr) RandomPeer() (*grpc.ClientConn, bool) {
	if len(c.peers) == 0 {
		return nil, false
	}

	target := c.targets[rand.Intn(len(c.targets))]
	return c.peers[target], true
}
