#!/bin/sh

go build ./cmd/replds || exit 1

pids=
./replds server --grpc-addr :12100 --http-addr :12101 --peer localhost:12200 --peer localhost:12300 --store ./test1 &
pids="$pids $!"
./replds server --grpc-addr :12200 --http-addr :12201 --peer localhost:12100 --peer localhost:12300 --store ./test2 &
pids="$pids $!"
./replds server --grpc-addr :12300 --http-addr :12301 --peer localhost:12100 --peer localhost:12200 --store ./test3 &
pids="$pids $!"

echo "Press Enter to stop test >"
echo
read ans
kill $pids
