package replds

import (
	"git.autistici.org/ai3/tools/replds2/common"
	pb "git.autistici.org/ai3/tools/replds2/proto"
)

type watcherManager struct {
	watchers map[string]chan []*pb.Node
}

func newWatcherManager() *watcherManager {
	return &watcherManager{
		watchers: make(map[string]chan []*pb.Node),
	}
}

type watcher struct {
	path string
	ch   <-chan []*pb.Node
	mgr  *watcherManager
}

func (m *watcherManager) Has(path string) bool {
	_, ok := m.watchers[path]
	return ok
}

func (m *watcherManager) notify(b *common.NotifyBatch) {
	b.Apply(func(path string, nodes []*pb.Node) {
		m.watchers[path] <- nodes
	})
}

func (m *watcherManager) newWatcher(path string) *watcher {
	ch := make(chan []*pb.Node, 1)
	m.watchers[path] = ch
	return &watcher{
		mgr:  m,
		path: path,
		ch:   ch,
	}
}

func (w *watcher) Close() {
	delete(w.mgr.watchers, w.path)
}

func (w *watcher) Next() <-chan []*pb.Node {
	return w.ch
}
