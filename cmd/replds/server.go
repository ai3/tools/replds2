package main

import (
	"context"
	"crypto/tls"
	"flag"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	replds "git.autistici.org/ai3/tools/replds2"
	"git.autistici.org/ai3/tools/replds2/acl"
	"git.autistici.org/ai3/tools/replds2/common"
	pb "git.autistici.org/ai3/tools/replds2/proto"
	"git.autistici.org/ai3/tools/replds2/store/memlog"
	"github.com/google/subcommands"
	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_auth "github.com/grpc-ecosystem/go-grpc-middleware/auth"
	grpc_prometheus "github.com/grpc-ecosystem/go-grpc-prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"golang.org/x/sync/errgroup"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/credentials/insecure"

	_ "net/http/pprof"
)

type serverCommand struct {
	grpcAddr      string
	httpAddr      string
	sslCert       string
	sslKey        string
	clientSSLCert string
	clientSSLKey  string
	sslCA         string
	acls          string
	peers         listFlag
	storePath     string
}

func init() {
	subcommands.Register(&serverCommand{}, "")

	grpc.EnableTracing = true
}

func (c *serverCommand) Name() string     { return "server" }
func (c *serverCommand) Synopsis() string { return "run the GRPC server" }
func (c *serverCommand) Usage() string {
	return `server:
        Start the GRPC server.

`
}

func (c *serverCommand) SetFlags(f *flag.FlagSet) {
	f.StringVar(&c.grpcAddr, "grpc-addr", getenv("REPLDS_GRPC_ADDR", ":3636"), "`address` to listen on")
	f.StringVar(&c.httpAddr, "http-addr", getenv("REPLDS_HTTP_ADDR", ":3638"), "address to listen on for the HTTP API (optional)")
	f.StringVar(&c.sslCert, "ssl-cert", os.Getenv("REPLDS_SSL_CERT"), "server SSL certificate `path`")
	f.StringVar(&c.sslKey, "ssl-key", os.Getenv("REPLDS_SSL_KEY"), "server SSL private key `path`")
	f.StringVar(&c.clientSSLCert, "client-ssl-cert", os.Getenv("REPLDS_SSL_CLIENT_CERT"), "client SSL certificate `path`")
	f.StringVar(&c.clientSSLKey, "client-ssl-key", os.Getenv("REPLDS_SSL_CLIENT_KEY"), "client SSL private key `path`")
	f.StringVar(&c.sslCA, "ssl-ca", os.Getenv("REPLDS_SSL_CA"), "SSL CA `path`")
	f.StringVar(&c.acls, "acls", os.Getenv("REPLDS_ACLS"), "`path` to file with ACL specs")
	f.StringVar(&c.storePath, "store", os.Getenv("REPLDS_STORE"), "`path` for local file storage")

	if s := os.Getenv("REPLDS_PEERS"); s != "" {
		c.peers = listFlag(strings.Split(s, ","))
	}
	f.Var(&c.peers, "peer", "peer `addr`esses (can be specified multiple times)")
}

func (c *serverCommand) grpcDialOptions() ([]grpc.DialOption, error) {
	var creds credentials.TransportCredentials
	if c.clientSSLCert != "" && c.clientSSLKey != "" && c.sslCA != "" {
		tlsconf, err := clientTLSConfig(c.clientSSLCert, c.clientSSLKey, c.sslCA)
		if err != nil {
			return nil, err
		}
		creds = credentials.NewTLS(tlsconf)
	} else {
		creds = insecure.NewCredentials()
	}
	return []grpc.DialOption{
		grpc.WithTransportCredentials(creds),
	}, nil
}

func (c *serverCommand) Execute(ctx context.Context, f *flag.FlagSet, args ...interface{}) subcommands.ExitStatus {
	if f.NArg() > 0 {
		log.Printf("Too many arguments")
		return subcommands.ExitUsageError
	}
	if c.storePath == "" {
		log.Printf("Must specify --store")
		return subcommands.ExitUsageError
	}

	// Load ACLs, if specified, otherwise fallback to a NULL ACL
	// that allows all access to anyone.
	var acls replds.ACLMgr
	if c.acls != "" {
		var err error
		acls, err = acl.Load(c.acls)
		if err != nil {
			log.Printf("error loading ACLs: %v", err)
			return subcommands.ExitFailure
		}
	} else {
		log.Printf("warning: running with completely open ACLs!")
		acls = acl.NewNullManager()
	}

	// Open the file-backed data store.
	store, err := memlog.NewMemFileStore(c.storePath)
	if err != nil {
		log.Printf("error initializing storage: %v", err)
		return subcommands.ExitFailure
	}
	defer store.Close()

	// Prepare GRPC connections to the other peers.
	opts, err := c.grpcDialOptions()
	if err != nil {
		log.Printf("error in GRPC options: %v", err)
		return subcommands.ExitFailure
	}
	cluster, err := replds.NewGRPCCluster(c.peers, opts...)
	if err != nil {
		log.Printf("error initializing GRPC cluster: %v", err)
		return subcommands.ExitFailure
	}

	// Build the controlling context that we'll cancel when
	// receiving a termination signal.
	sctx, cancel := context.WithCancel(ctx)
	sigCh := make(chan os.Signal, 1)
	go func() {
		<-sigCh
		log.Printf("received termination signal, exiting")
		cancel()
	}()
	signal.Notify(sigCh, syscall.SIGINT, syscall.SIGTERM)

	// Build the replds.Server and run it.
	server := replds.NewServer(sctx, cluster, store, acls)

	err = c.runServer(sctx, server)
	if err != nil {
		log.Printf("error: %v", err)
		return subcommands.ExitFailure
	}
	return subcommands.ExitSuccess
}

func (c *serverCommand) runServer(ctx context.Context, server *replds.Server) error {
	g, ictx := errgroup.WithContext(ctx)

	var tlsconf *tls.Config
	if c.sslCert != "" && c.sslKey != "" && c.sslCA != "" {
		var err error
		tlsconf, err = serverTLSConfig(c.sslCert, c.sslKey, c.sslCA)
		if err != nil {
			return err
		}
	}

	// Background sync thread.
	g.Go(func() error {
		server.RunSyncThread(ictx)
		return nil
	})

	// Initialize the HTTP server.
	g.Go(func() error {
		// Use the default http ServeMux.
		http.Handle("/metrics", promhttp.Handler())

		httpSrv := &http.Server{
			Addr:              c.httpAddr,
			TLSConfig:         tlsconf,
			Handler:           nil,
			ReadTimeout:       10 * time.Second,
			ReadHeaderTimeout: 30 * time.Second,
			IdleTimeout:       30 * time.Second,
			WriteTimeout:      10 * time.Second,
		}

		return runHTTPServerWithContext(ictx, httpSrv)
	})

	// Initialize the GRPC server.
	g.Go(func() error {
		var opts = []grpc.ServerOption{
			grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(
				grpc_prometheus.UnaryServerInterceptor,
			)),
			grpc.StreamInterceptor(
				grpc_prometheus.StreamServerInterceptor,
			),
		}
		if tlsconf != nil {
			opts = append(opts, grpc.StreamInterceptor(grpc_auth.StreamServerInterceptor(common.TLSAuthFunc)))
			opts = append(opts, grpc.UnaryInterceptor(grpc_auth.UnaryServerInterceptor(common.TLSAuthFunc)))
			opts = append(opts, grpc.Creds(credentials.NewTLS(tlsconf)))
		}

		grpcSrv := grpc.NewServer(opts...)
		pb.RegisterInternalReplServer(grpcSrv, server)
		pb.RegisterRepldsServer(grpcSrv, server)
		grpc_prometheus.Register(grpcSrv)

		return runGRPCServerWithContext(ictx, grpcSrv, c.grpcAddr)
	})

	return g.Wait()
}

// Run a GRPC server controlled by a Context.
func runGRPCServerWithContext(ctx context.Context, server *grpc.Server, addr string) error {
	l, err := net.Listen("tcp", addr)
	if err != nil {
		return err
	}
	defer l.Close()

	go func() {
		<-ctx.Done()
		server.GracefulStop()
	}()

	return server.Serve(l)
}

// Run a HTTP server controlled by a Context.
func runHTTPServerWithContext(ctx context.Context, server *http.Server) error {
	go func() {
		<-ctx.Done()
		sctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()
		if err := server.Shutdown(sctx); err != nil {
			server.Close() // nolint: errcheck
		}
	}()

	err := server.ListenAndServe()
	// Ignore the ErrServerClosed error, since we're presumably
	// shutting down on purpose.
	if err == http.ErrServerClosed {
		err = nil
	}
	return err
}
