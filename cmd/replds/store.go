package main

import (
	"context"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"

	pb "git.autistici.org/ai3/tools/replds2/proto"
	"github.com/google/subcommands"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/protobuf/types/known/timestamppb"
)

type storeCommand struct {
	sslCert    string
	sslKey     string
	sslCA      string
	serverAddr string
}

func init() {
	subcommands.Register(&storeCommand{}, "")
}

func (c *storeCommand) Name() string     { return "store" }
func (c *storeCommand) Synopsis() string { return "store data on the database" }
func (c *storeCommand) Usage() string {
	return `store <local-path> <remote-path>:
        Store data on the remote database.

`
}

func (c *storeCommand) SetFlags(f *flag.FlagSet) {
	f.StringVar(&c.serverAddr, "server", "", "server `address` (host:port)")
	f.StringVar(&c.sslCert, "ssl-cert", "", "client SSL certificate `path`")
	f.StringVar(&c.sslKey, "ssl-key", "", "client SSL private key `path`")
	f.StringVar(&c.sslCA, "ssl-ca", "", "SSL CA `path`")
}

func (c *storeCommand) grpcDialOptions() ([]grpc.DialOption, error) {
	var creds credentials.TransportCredentials
	if c.sslCert != "" && c.sslKey != "" && c.sslCA != "" {
		tlsconf, err := clientTLSConfig(c.sslCert, c.sslKey, c.sslCA)
		if err != nil {
			return nil, err
		}
		creds = credentials.NewTLS(tlsconf)
	} else {
		creds = insecure.NewCredentials()
	}
	return []grpc.DialOption{
		grpc.WithTransportCredentials(creds),
	}, nil
}

func (c *storeCommand) Execute(ctx context.Context, f *flag.FlagSet, args ...interface{}) subcommands.ExitStatus {
	if f.NArg() != 2 {
		log.Printf("Wrong number of arguments")
		return subcommands.ExitUsageError
	}
	if c.serverAddr == "" {
		log.Printf("Must specify --server")
		return subcommands.ExitUsageError
	}

	// Dial the GRPC server.
	opts, err := c.grpcDialOptions()
	if err != nil {
		log.Printf("error in GRPC options: %v", err)
		return subcommands.ExitFailure
	}

	conn, err := grpc.Dial(c.serverAddr, opts...)
	if err != nil {
		log.Printf("error establishing GRPC connection: %v", err)
		return subcommands.ExitFailure
	}
	stub := pb.NewRepldsClient(conn)

	localPath := f.Arg(0)
	remotePath := f.Arg(1)
	if !strings.HasPrefix(remotePath, "/") {
		remotePath = "/" + remotePath
	}

	// Build a list of Nodes by looking at what's on the filesystem.
	nodes, err := buildNodes(localPath, remotePath)
	if err != nil {
		log.Printf("error: %v", err)
		return subcommands.ExitFailure
	}

	_, err = stub.Store(ctx, &pb.StoreRequest{Nodes: nodes})
	if err != nil {
		log.Printf("rpc error: %v", err)
		return subcommands.ExitFailure
	}

	return subcommands.ExitSuccess
}

func buildNodes(localPath, remotePath string) ([]*pb.Node, error) {
	var out []*pb.Node
	err := filepath.Walk(localPath, func(path string, fi os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if !fi.Mode().IsRegular() {
			return nil
		}

		data, err := ioutil.ReadFile(path)
		if err != nil {
			return fmt.Errorf("reading %s: %w", path, err)
		}

		ts := fi.ModTime()
		path = filepath.Join(remotePath, path[len(localPath):])
		out = append(out, &pb.Node{
			Path:      path,
			Data:      data,
			Version:   int64(ts.Unix()),
			Timestamp: timestamppb.New(ts),
		})

		return nil
	})
	return out, err
}
