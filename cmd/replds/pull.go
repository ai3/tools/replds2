package main

import (
	"context"
	"flag"
	"log"
	"os"
	"os/signal"
	"strings"
	"syscall"

	"git.autistici.org/ai3/tools/replds2/store/memlog"
	"git.autistici.org/ai3/tools/replds2/watcher"
	"github.com/google/subcommands"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/credentials/insecure"
)

type pullCommand struct {
	sslCert      string
	sslKey       string
	sslCA        string
	storePath    string
	triggersPath string
	serverAddr   string
}

func init() {
	subcommands.Register(&pullCommand{}, "")
}

func (c *pullCommand) Name() string     { return "pull" }
func (c *pullCommand) Synopsis() string { return "run a replication client" }
func (c *pullCommand) Usage() string {
	return `pull <subpath>:
        Run a replication client for part of the tree.

`
}

func (c *pullCommand) SetFlags(f *flag.FlagSet) {
	f.StringVar(&c.serverAddr, "server", os.Getenv("REPLDS_SERVER"), "server `address` (host:port)")
	f.StringVar(&c.sslCert, "ssl-cert", os.Getenv("REPLDS_SSL_CLIENT_CERT"), "client SSL certificate `path`")
	f.StringVar(&c.sslKey, "ssl-key", os.Getenv("REPLDS_SSL_CLIENT_KEY"), "client SSL private key `path`")
	f.StringVar(&c.sslCA, "ssl-ca", os.Getenv("REPLDS_SSL_CA"), "SSL CA `path`")
	f.StringVar(&c.storePath, "store", os.Getenv("REPLDS_STORE"), "`path` for local file storage")
	f.StringVar(&c.triggersPath, "triggers-dir", os.Getenv("REPLDS_TRIGGERS_DIR"), "`path` where trigger configs are stored")
}

func (c *pullCommand) grpcDialOptions() ([]grpc.DialOption, error) {
	var creds credentials.TransportCredentials
	if c.sslCert != "" && c.sslKey != "" && c.sslCA != "" {
		tlsconf, err := clientTLSConfig(c.sslCert, c.sslKey, c.sslCA)
		if err != nil {
			return nil, err
		}
		creds = credentials.NewTLS(tlsconf)
	} else {
		creds = insecure.NewCredentials()
	}
	return []grpc.DialOption{
		grpc.WithTransportCredentials(creds),
	}, nil
}

func (c *pullCommand) Execute(ctx context.Context, f *flag.FlagSet, args ...interface{}) subcommands.ExitStatus {
	if f.NArg() != 1 {
		log.Printf("Wrong number of arguments")
		return subcommands.ExitUsageError
	}
	if c.serverAddr == "" {
		log.Printf("Must specify --server")
		return subcommands.ExitUsageError
	}
	if c.storePath == "" {
		log.Printf("Must specify --store")
		return subcommands.ExitUsageError
	}

	// Load all triggers from their JSON configuration directory.
	var triggers watcher.TriggerManager
	if c.triggersPath != "" {
		var err error
		triggers, err = watcher.LoadTriggersFromDir(c.triggersPath)
		if err != nil {
			log.Printf("error loading triggers: %v", err)
			return subcommands.ExitFailure
		}
	}

	// Create a file-backed store for the local target path.
	store, err := memlog.NewMemFileStore(c.storePath)
	if err != nil {
		log.Printf("error initializing storage: %v", err)
		return subcommands.ExitFailure
	}
	defer store.Close()

	// Dial the GRPC server.
	opts, err := c.grpcDialOptions()
	if err != nil {
		log.Printf("error in GRPC options: %v", err)
		return subcommands.ExitFailure
	}

	conn, err := grpc.Dial(c.serverAddr, opts...)
	if err != nil {
		log.Printf("error establishing GRPC connection: %v", err)
		return subcommands.ExitFailure
	}

	// Create the Watcher, and run it with a controlling Context
	// that allows us to stop everything upon receiving a
	// termination signal.
	prefix := f.Arg(0)
	if !strings.HasPrefix(prefix, "/") {
		prefix = "/" + prefix
	}
	w := watcher.New(conn, store, prefix, triggers)

	wctx, cancel := context.WithCancel(ctx)
	sigCh := make(chan os.Signal, 1)
	go func() {
		<-sigCh
		log.Printf("received termination signal, exiting")
		cancel()
	}()
	signal.Notify(sigCh, syscall.SIGINT, syscall.SIGTERM)

	log.Printf("starting watcher")
	w.Run(wctx)

	return subcommands.ExitSuccess
}
