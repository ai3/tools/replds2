package main

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

func getenv(key, dfl string) string {
	if s := os.Getenv(key); s != "" {
		return s
	}
	return dfl
}

type listFlag []string

func (l *listFlag) Set(s string) error {
	*l = append(*l, s)
	return nil
}

func (l listFlag) String() string {
	return strings.Join(l, ",")
}

func clientTLSConfig(sslCert, sslKey, sslCA string) (*tls.Config, error) {
	cert, err := tls.LoadX509KeyPair(sslCert, sslKey)
	if err != nil {
		return nil, err
	}
	ca, err := loadCA(sslCA)
	if err != nil {
		return nil, err
	}
	return &tls.Config{
		Certificates: []tls.Certificate{cert},
		MinVersion:   tls.VersionTLS12,
		RootCAs:      ca,
	}, nil
}

func serverTLSConfig(sslCert, sslKey, sslCA string) (*tls.Config, error) {
	cert, err := tls.LoadX509KeyPair(sslCert, sslKey)
	if err != nil {
		return nil, err
	}
	ca, err := loadCA(sslCA)
	if err != nil {
		return nil, err
	}
	return &tls.Config{
		Certificates:             []tls.Certificate{cert},
		MinVersion:               tls.VersionTLS12,
		PreferServerCipherSuites: true,
		ClientAuth:               tls.RequireAndVerifyClientCert,
		ClientCAs:                ca,
	}, nil
}

func loadCA(path string) (*x509.CertPool, error) {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	cas := x509.NewCertPool()
	if !cas.AppendCertsFromPEM(data) {
		return nil, fmt.Errorf("no certificates could be parsed in %s", path)
	}
	return cas, nil
}
