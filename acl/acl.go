package acl

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"strings"

	"git.autistici.org/ai3/tools/replds2/common"
)

type Op int

const (
	OpRead = iota
	OpWrite
	OpPeer
)

func (op Op) String() string {
	switch op {
	case OpRead:
		return "READ"
	case OpWrite:
		return "WRITE"
	case OpPeer:
		return "PEER"
	}
	return "<ERROR>"
}

type NullManager struct{}

func NewNullManager() *NullManager { return new(NullManager) }

func (m *NullManager) Check(identity string, op Op, path string) bool { return true }

type Entry struct {
	Identity string
	Path     string
	Op       Op
}

type Manager struct {
	acls map[Entry]struct{}
}

func NewManager(acls []Entry) *Manager {
	m := &Manager{acls: make(map[Entry]struct{})}
	for _, e := range acls {
		m.acls[e] = struct{}{}
	}
	return m
}

func (m *Manager) Check(identity string, op Op, path string) bool {
	e := Entry{Identity: identity, Op: op}
	for _, p := range common.PathPrefixes(path) {
		e.Path = p
		if _, ok := m.acls[e]; ok {
			return true
		}
	}
	return false
}

// Load ACL rules from a text file. The file should contain one rule
// per line, either:
//
//   <identity> <path> <op(READ|WRITE)>
//
// or:
//
//   <identity> <op(PEER)>
//
// (PEER ACLs have no path). Empty lines and lines starting with a #
// are ignored.
//
func Load(path string) (*Manager, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	var acls []Entry
	scanner := bufio.NewScanner(f)
	lineno := 0
	for scanner.Scan() {
		lineno++
		line := strings.TrimSpace(scanner.Text())
		if strings.HasPrefix(line, "#") || line == "" {
			continue
		}
		e, err := parseEntry(line)
		if err != nil {
			return nil, fmt.Errorf("syntax error at %s:%d: %w", path, lineno, err)
		}
		acls = append(acls, e)
	}

	if err := scanner.Err(); err != nil {
		return nil, err
	}

	return NewManager(acls), nil
}

func parseEntry(line string) (e Entry, err error) {
	fields := strings.Fields(line)

	switch len(fields) {
	case 2:
		e.Op, err = parseOp(fields[1])
		if err != nil {
			return
		}
		if e.Op != OpPeer {
			err = errors.New("non-PEER acl without path")
			return
		}

	case 3:
		e.Path = fields[1]
		e.Op, err = parseOp(fields[2])
		if err != nil {
			return
		}
		if e.Op != OpRead && e.Op != OpWrite {
			err = errors.New("acl op is not 'read' or 'write'")
			return
		}

	default:
		err = errors.New("not enough fields")
		return
	}

	e.Identity = fields[0]
	return
}

func parseOp(s string) (op Op, err error) {
	switch strings.ToLower(s) {
	case "read", "r":
		op = OpRead
	case "write", "w":
		op = OpWrite
	case "peer":
		op = OpPeer
	default:
		err = fmt.Errorf("unknown op '%s'", s)
	}
	return
}
