package acl

import "testing"

func TestACL(t *testing.T) {
	acls := NewManager([]Entry{
		{Identity: "user", Path: "/home/user", Op: OpWrite},
		{Identity: "user", Path: "/home", Op: OpRead},
		{Identity: "admin", Path: "/", Op: OpRead},
		{Identity: "admin", Path: "/", Op: OpWrite},
	})

	testdata := []struct {
		identity, path string
		op             Op
		expectedOk     bool
	}{
		{"user", "/home/user/foo", OpWrite, true},
		{"user", "/home/other-user/bar", OpWrite, false},
		{"user", "/foo/bar", OpWrite, false},
		{"user", "/foo/bar", OpRead, false},
		{"admin", "/home/user/foo", OpWrite, true},
		{"admin", "/foo/bar", OpWrite, true},
		{"admin", "/foo/bar", OpRead, true},
		{"nobody", "/home/user/foo", OpRead, false},
		{"nobody", "/foo/bar", OpRead, false},
		{"nobody", "/foo/bar", OpWrite, false},
	}

	for _, td := range testdata {
		ok := acls.Check(td.identity, td.op, td.path)
		if ok != td.expectedOk {
			t.Errorf("identity=%s path=%s op=%s  got ok=%v, expected ok=%v",
				td.identity, td.path, td.op, ok, td.expectedOk)
		}
	}
}
