FROM docker.io/library/golang:1.20.1 AS build

WORKDIR /src
ADD . /src
RUN go build -ldflags='-extldflags=-static -w -s' -tags osusergo,netgo ./cmd/replds

FROM scratch
COPY --from=build /src/replds /replds

ENTRYPOINT ["/replds"]
