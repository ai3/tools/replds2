package common

import (
	pb "git.autistici.org/ai3/tools/replds2/proto"
)

type NotifySource interface {
	Has(string) bool
}

type NotifyBatch struct {
	src   NotifySource
	Nodes map[string][]*pb.Node
}

func NewNotifyBatch(src NotifySource) *NotifyBatch {
	return &NotifyBatch{
		src:   src,
		Nodes: make(map[string][]*pb.Node),
	}
}

func (b *NotifyBatch) Add(node *pb.Node) {
	for _, path := range PathPrefixes(node.Path) {
		if b.src.Has(path) {
			b.Nodes[path] = append(b.Nodes[path], node)
		}
	}
}

func (b *NotifyBatch) Apply(f func(string, []*pb.Node)) {
	for path, nodes := range b.Nodes {
		f(path, nodes)
	}
}
