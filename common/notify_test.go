package common

import (
	"testing"

	pb "git.autistici.org/ai3/tools/replds2/proto"
)

type testSource map[string]func()

func (s testSource) Has(path string) bool {
	_, ok := s[path]
	return ok
}

func TestNotifyBatch(t *testing.T) {
	notified := make(map[string]bool)
	setNotified := func(pfx string) func() {
		return func() {
			notified[pfx] = true
		}
	}
	triggers := map[string]func(){
		"/test/one/f1": setNotified("/test/one/f1"),
		"/test":        setNotified("/test"),
		"/":            setNotified("/"),
	}

	b := NewNotifyBatch(testSource(triggers))
	b.Add(&pb.Node{Path: "/test/one/f1"})
	b.Add(&pb.Node{Path: "/test/two"})
	b.Apply(func(path string, nodes []*pb.Node) {
		triggers[path]()
	})

	if !notified["/"] {
		t.Error("no notification for /")
	}
	if !notified["/test"] {
		t.Error("no notification for /test")
	}
	if !notified["/test/one/f1"] {
		t.Error("no notification for /test/one/f1")
	}
}
