package common

import (
	"context"
	"crypto/x509"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/peer"
	"google.golang.org/grpc/status"
)

type authIdentityType int

var authIdentityKey authIdentityType = 1

func getPeerCertificateFromContext(ctx context.Context) (*x509.Certificate, bool) {
	p, ok := peer.FromContext(ctx)
	if !ok {
		return nil, false
	}
	mtls, ok := p.AuthInfo.(credentials.TLSInfo)
	if !ok {
		return nil, false
	}
	if len(mtls.State.PeerCertificates) == 0 {
		return nil, false
	}
	return mtls.State.PeerCertificates[0], true
}

// TLSAuthFunc is an AuthFunc for grpc-middleware/grpc_auth.
func TLSAuthFunc(ctx context.Context) (context.Context, error) {
	// get client tls info
	cert, ok := getPeerCertificateFromContext(ctx)
	if !ok {
		return nil, status.Errorf(codes.Unauthenticated, "no client certificate")
	}
	return context.WithValue(ctx, authIdentityKey, cert.Subject.CommonName), nil
}

func GetAuthIdentity(ctx context.Context) string {
	if s, ok := ctx.Value(authIdentityKey).(string); ok {
		return s
	}
	return ""
}
