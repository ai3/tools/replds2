package common

import "strings"

func PathPrefixes(path string) []string {
	parts := strings.Split(path, "/")
	n := len(parts)
	out := make([]string, 0, n)
	for i := 0; i < n; i++ {
		out = append(out, joinPath(parts[:n-i]))
	}
	return out
}

func joinPath(parts []string) string {
	s := strings.Join(parts, "/")
	if s == "" {
		s = "/"
	}
	return s
}
