package replds

import (
	"context"
	"errors"
	"fmt"
	"log"
	"sync"
	"time"

	"git.autistici.org/ai3/tools/replds2/acl"
	"git.autistici.org/ai3/tools/replds2/common"
	pb "git.autistici.org/ai3/tools/replds2/proto"
	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var emptyResponse = new(empty.Empty)

// Store is the interface to the Node database.
type Store interface {
	AddNode(*pb.Node) (bool, error)
	Summary() *pb.NodeStateSummary
	TreeDiff(*pb.NodeStateSummary, string) []*pb.Node
}

// ACLMgr is an ACL manager that can check access rules.
type ACLMgr interface {
	Check(string, acl.Op, string) bool
}

// ClusterMgr holds state about the service cluster network layout.
type ClusterMgr interface {
	RandomPeer() (*grpc.ClientConn, bool)
}

// Server is the main replds GRPC server.
type Server struct {
	gctx         context.Context
	cluster      ClusterMgr
	acl          ACLMgr
	syncInterval time.Duration

	mx       sync.Mutex
	nodes    Store
	watchers *watcherManager

	*pb.UnimplementedRepldsServer
	*pb.UnimplementedInternalReplServer
}

// NewServer creates a new replds Server. We need a global context to
// gracefully stop clients on server-streaming connections.
func NewServer(ctx context.Context, cluster ClusterMgr, store Store, acl ACLMgr) *Server {
	return &Server{
		gctx:         ctx,
		cluster:      cluster,
		acl:          acl,
		nodes:        store,
		watchers:     newWatcherManager(),
		syncInterval: 10 * time.Second,
	}
}

func (s *Server) addNodes(nodes []*pb.Node) (err error) {
	s.mx.Lock()
	defer s.mx.Unlock()

	// Abort on the first error, but always notify. Notifications
	// are only triggered if AddNode reported a change.
	var ok bool
	b := common.NewNotifyBatch(s.watchers)

	for _, node := range nodes {
		ok, err = s.nodes.AddNode(node)
		if err != nil {
			break
		}
		if ok {
			b.Add(node)
		}
	}

	s.watchers.notify(b)
	return
}

func (s *Server) Store(ctx context.Context, req *pb.StoreRequest) (*empty.Empty, error) {
	for _, node := range req.Nodes {
		if !s.acl.Check(common.GetAuthIdentity(ctx), acl.OpWrite, node.Path) {
			return nil, status.Error(codes.PermissionDenied, "unauthorized")
		}
	}

	if err := s.addNodes(req.Nodes); err != nil {
		return nil, err
	}
	return emptyResponse, nil
}

func (s *Server) SyncNodes(ctx context.Context, req *pb.SyncNodesRequest) (*pb.SyncNodesResponse, error) {
	if !s.acl.Check(common.GetAuthIdentity(ctx), acl.OpPeer, "") {
		return nil, status.Error(codes.PermissionDenied, "unauthorized")
	}

	s.mx.Lock()
	diff := s.nodes.TreeDiff(req.Summary, "/")
	s.mx.Unlock()
	return &pb.SyncNodesResponse{
		Nodes: diff,
	}, nil
}

func (s *Server) Watch(req *pb.WatchRequest, stream pb.Replds_WatchServer) error {
	ctx, cancel := mergeContext(stream.Context(), s.gctx)
	defer cancel()

	if !s.acl.Check(common.GetAuthIdentity(ctx), acl.OpRead, req.BasePath) {
		return status.Error(codes.PermissionDenied, "unauthorized")
	}

	// Compute initial diff.
	s.mx.Lock()
	diff := s.nodes.TreeDiff(req.Summary, req.BasePath)
	w := s.watchers.newWatcher(req.BasePath)
	s.mx.Unlock()
	defer func() {
		s.mx.Lock()
		w.Close()
		s.mx.Unlock()
	}()

	// Send initial diff (while not holding the lock).
	if len(diff) > 0 {
		if err := stream.Send(&pb.WatchResponse{Nodes: diff}); err != nil {
			return err
		}
	}

	for {
		select {
		case <-ctx.Done():
			return ctx.Err()

		case batch := <-w.Next():
			if err := stream.Send(&pb.WatchResponse{Nodes: batch}); err != nil {
				return err
			}
		}
	}
}

func (s *Server) pingRandomPeer(ctx context.Context) error {
	peer, ok := s.cluster.RandomPeer()
	if !ok {
		return errors.New("no peers to ping")
	}

	s.mx.Lock()
	req := &pb.SyncNodesRequest{
		Summary: s.nodes.Summary(),
	}
	s.mx.Unlock()

	svc := pb.NewInternalReplClient(peer)
	resp, err := svc.SyncNodes(ctx, req)
	if err != nil {
		return fmt.Errorf("%s: %w", peer.Target(), err)
	}
	return s.addNodes(resp.Nodes)
}

// RunSyncThread runs the background ping goroutine for as long as the
// passed Context remains valid.
func (s *Server) RunSyncThread(ctx context.Context) {
	timer := time.NewTicker(s.syncInterval)
	defer timer.Stop()

	for {
		sctx, cancel := context.WithTimeout(ctx, s.syncInterval)
		err := s.pingRandomPeer(sctx)
		cancel()
		if err != nil {
			log.Printf("sync error: %v", err)
		}

		select {
		case <-timer.C:
		case <-ctx.Done():
			return
		}
	}
}

func mergeContext(a, b context.Context) (context.Context, context.CancelFunc) {
	mctx, mcancel := context.WithCancel(a) // will cancel if `a` cancels

	go func() {
		select {
		case <-mctx.Done(): // don't leak go-routine on clean gRPC run
		case <-b.Done():
			mcancel() // b canceled, so cancel mctx
		}
	}()

	return mctx, mcancel
}
