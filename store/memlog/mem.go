package memlog

import (
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"

	pb "git.autistici.org/ai3/tools/replds2/proto"
	"github.com/armon/go-radix"
)

// A combination of in-memory radix-tree, and disk-based log.
type logTree struct {
	*radix.Tree
	*appendLog
}

func (t *logTree) get(path string) (*pb.Node, bool) {
	value, ok := t.Tree.Get(path)
	if ok {
		return value.(*pb.Node), true
	}
	return nil, false
}

// Add an element to the db, without adding it to the log. This is
// meant to be called by the log layer itself.
func (t *logTree) logPut(node *pb.Node) error {
	t.Tree.Insert(node.Path, node)
	return nil
}

// Add an element to the db.
func (t *logTree) put(node *pb.Node) error {
	t.Tree.Insert(node.Path, node)
	return t.appendLog.Write(node)
}

// Iterate over all elements in the db, calling f. If f returns an
// error, the iteration stops.
func (t *logTree) dump(f func(*pb.Node) error) (err error) {
	t.Tree.Walk(func(path string, value interface{}) bool {
		err = f(value.(*pb.Node))
		return err != nil
	})
	return
}

func newLogTree(path string) (*logTree, error) {
	tree := &logTree{Tree: radix.New()}
	log, err := openLog(path, tree.logPut, tree.dump)
	if err != nil {
		return nil, err
	}
	tree.appendLog = log
	return tree, nil
}

// MemStore is an in-memory database of Node objects, indexed by their
// path. It utilizes a radix tree (compressed trie) to organize the
// nodes, so that scanning by path prefix is fast.
//
// In-memory data is persisted to disk using an append-only log,
// checkpointed on startup and auto-rotated.
type MemStore struct {
	tree *logTree
}

// NewMemStore returns a MemStore backed by an append-only log at the
// specified path (which must be a directory). The log directory is
// created if it does not exist.
func NewMemStore(path string) (*MemStore, error) {
	tree, err := newLogTree(path)
	if err != nil {
		return nil, err
	}
	return &MemStore{
		tree: tree,
	}, nil
}

// AddNode adds a node to the database. Attempts to add nodes which
// have newer versions in the database will be ignored. The function
// returns true if the database was actually modified.
func (m *MemStore) AddNode(node *pb.Node) (bool, error) {
	if old, ok := m.tree.get(node.Path); ok && old.Version >= node.Version {
		return false, nil
	}
	return true, m.tree.put(node)
}

// Summary returns a NodeStateSummary with all the metadata of the
// Node objects in the database.
func (m *MemStore) Summary() *pb.NodeStateSummary {
	summary := &pb.NodeStateSummary{
		NodeMeta: make([]*pb.NodeMeta, 0, m.tree.Len()),
	}
	m.tree.Walk(func(path string, value interface{}) bool {
		summary.NodeMeta = append(summary.NodeMeta, value.(*pb.Node).Meta())
		return false
	})
	return summary
}

// TreeDiff compares part (or all) of the database with a summary, and
// returns all the nodes contained in the database which are missing
// in the summary.
func (m *MemStore) TreeDiff(summary *pb.NodeStateSummary, prefix string) []*pb.Node {
	if !strings.HasSuffix(prefix, "/") {
		prefix += "/"
	}

	// Convert the summary to a map, so we can look it up quickly.
	tmp := make(map[string]int64)
	for _, nm := range summary.NodeMeta {
		tmp[nm.Path] = nm.Version
	}

	// Now scan our tree and accumulate missing elements.
	var out []*pb.Node
	m.tree.WalkPrefix(prefix, func(path string, value interface{}) bool {
		node := value.(*pb.Node)
		version, ok := tmp[path]
		if !ok || version < node.Version {
			out = append(out, node)
		}
		return false
	})
	return out
}

// Close the data store and all its associated resources.
func (m *MemStore) Close() {
	m.tree.Close()
}

// MemFileStore is just like MemStore, but with node data backed by
// the file system. Only the node metadata will be kept in memory and
// in the append-only logs, while the node data itself will be saved
// to disk, in files named after each node's path.
//
// The result is that MemFileStore produces a file hierarchy that
// matches exactly what is in the database, and can thus be used by
// external programs.
//
type MemFileStore struct {
	*MemStore

	path string
}

// NewMemFileStore creates a new MemFileStore based at 'path'. The
// database will be stored in a subdirectory of path called '.db'.
func NewMemFileStore(path string) (*MemFileStore, error) {
	ms, err := NewMemStore(filepath.Join(path, ".db"))
	if err != nil {
		return nil, err
	}
	return &MemFileStore{
		MemStore: ms,
		path:     path,
	}, nil
}

func (m *MemFileStore) nodePath(node *pb.Node) string {
	return filepath.Join(m.path, node.Path)
}

func (m *MemFileStore) AddNode(node *pb.Node) (bool, error) {
	ok, _ := m.MemStore.AddNode(node.WithoutData()) // nolint: errcheck
	if !ok {
		return false, nil
	}

	path := m.nodePath(node)
	if node.Deleted {
		err := os.Remove(path)
		if os.IsNotExist(err) {
			err = nil
		}
		return false, err
	}

	if err := os.MkdirAll(filepath.Dir(path), 0700); err != nil {
		return false, err
	}
	log.Printf("writing %s data to %s", node.Path, path)
	return true, ioutil.WriteFile(path, node.Data, 0600)
}

func (m *MemFileStore) TreeDiff(summary *pb.NodeStateSummary, prefix string) []*pb.Node {
	nodes := m.MemStore.TreeDiff(summary, prefix)
	out := make([]*pb.Node, 0, len(nodes))
	for _, node := range nodes {
		if node.Deleted {
			out = append(out, node)
			continue
		}
		data, err := ioutil.ReadFile(m.nodePath(node))
		if err != nil {
			// This is generally bad as it means someone deleted
			// the file under our nose. Let's drop it from the
			// database too, so that it will be picked up again
			// the next time the Watcher restarts.
			//
			// TODO: to facilitate this process, we could abort.
			log.Printf("inconsistency: data for node %s not found, dropping node", node.Path)
			m.MemStore.tree.Delete(m.nodePath(node))
		}
		out = append(out, node.WithData(data))
	}
	return out
}
