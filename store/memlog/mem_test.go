package memlog

import (
	"io/ioutil"
	"os"
	"testing"

	pb "git.autistici.org/ai3/tools/replds2/proto"
	"github.com/google/go-cmp/cmp"
	"google.golang.org/protobuf/testing/protocmp"
)

var testNodes = []*pb.Node{
	&pb.Node{
		Path:    "/test/node1",
		Data:    []byte("this is node1"),
		Version: 2,
	},
	&pb.Node{
		Path:    "/test/node2",
		Data:    []byte("this is node2"),
		Version: 2,
	},
	&pb.Node{
		Path:    "/test/node3",
		Data:    []byte("this is node3"),
		Version: 2,
	},
}

type store interface {
	AddNode(*pb.Node) (bool, error)
	Summary() *pb.NodeStateSummary
	TreeDiff(*pb.NodeStateSummary, string) []*pb.Node
}

func runStoreTest(t *testing.T, ms store) {
	for _, node := range testNodes {
		if _, err := ms.AddNode(node); err != nil {
			t.Fatalf("AddNode(%s): %v", node.Path, err)
		}
	}

	// Verify the summary.
	summary := ms.Summary()
	if n := len(summary.NodeMeta); n != len(testNodes) {
		t.Fatalf("Summary() returned %d items, expected %d", n, len(testNodes))
	}

	// Run a TreeDiff against a subset of the test nodes.
	summary.NodeMeta = summary.NodeMeta[:2]
	nodes := ms.TreeDiff(summary, "/")
	if n := len(nodes); n != 1 {
		t.Fatalf("TreeDiff() returned %d nodes, expected 1", n)
	}
	if diffs := cmp.Diff(nodes[0], testNodes[2], protocmp.Transform()); diffs != "" {
		t.Fatalf("TreeDiff() returned unexpected result:\n%s", diffs)
	}

	// Try to push an update with an earlier version, and verify that it is discarded.
	ok, err := ms.AddNode(&pb.Node{
		Path:    "/test/node1",
		Version: 1,
		Deleted: true,
	})
	if err != nil {
		t.Fatal(err)
	}
	if ok {
		t.Fatal("AddNode() with old version reported successful change")
	}
	found := false
	for _, node := range ms.TreeDiff(&pb.NodeStateSummary{}, "/") {
		if node.Path == "/test/node1" {
			found = true
			if node.Version == 1 {
				t.Fatal("old version of /test/node1 was not discarded")
			}
			break
		}
	}
	if !found {
		t.Fatal("/test/node1 disappeared from node list")
	}
}

func TestMemStore(t *testing.T) {
	dir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir)

	ms, err := NewMemStore(dir)
	if err != nil {
		t.Fatal(err)
	}
	defer ms.Close()

	runStoreTest(t, ms)
}

func TestMemFileStore(t *testing.T) {
	dir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir)

	ms, err := NewMemFileStore(dir)
	if err != nil {
		t.Fatal(err)
	}
	defer ms.Close()

	runStoreTest(t, ms)
}
